package tutorial;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

@Embeddable
public class Address {

    @Column(name = "CITY")
    private String city;

    @Column(name = "STREET")
    private String street;

    @Column(name = "BUILDING_NO")
    private int buildingNo;

    @Column(name = "ZIP_CODE")
    private String zipCode;
}
