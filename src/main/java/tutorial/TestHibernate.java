package tutorial;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class TestHibernate {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        Student student1 = new Student("Filan1", "Fisteku1", 32, "filan1.fisteku1@gmail.com");
        Student student2 = new Student("Filan2", "Fisteku2", 20, "filan2.fisteku2@gmail.com");

        // per te ruajtur te dhena ne databaze
//        session.persist(student1);
//        session.persist(student2);

        // per te marre te dhena nga databaza
//        Student found = session.find(Student.class, 2);
//        System.out.println(found);

        // per te updetuar te dhena
//        student1.setAge(29);
//        session.merge(student1);

        // per te fshire te dhena
//        session.remove(student1);


        transaction.commit();
        session.close();
    }
}
