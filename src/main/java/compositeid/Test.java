package compositeid;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        EmployeeId id = new EmployeeId(2, "Filan");
        Employee employee = new Employee(id, 30, "filan@gmail.com");

        session.persist(employee);

        transaction.commit();
        session.close();
    }
}
