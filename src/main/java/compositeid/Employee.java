package compositeid;


import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;

@Entity
public class Employee {

    @EmbeddedId
    private EmployeeId employeeId;

    private int age;
    private String email;

    public Employee(EmployeeId employeeId, int age, String email) {
        this.employeeId = employeeId;
        this.age = age;
        this.email = email;
    }

    public EmployeeId getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(EmployeeId employeeId) {
        this.employeeId = employeeId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }
}
