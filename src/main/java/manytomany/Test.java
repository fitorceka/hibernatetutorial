package manytomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Test {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Exam.class)
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        Exam e1 = new Exam("Matematike", 30);
        Exam e2 = new Exam("Biologji", 20);
        Exam e3 = new Exam("Fizike", 25);
        Exam e4 = new Exam("Kimi", 25);
        Exam e5 = new Exam("Letersi", 50);

        Student s1 = new Student("Filan1", "Fistek1", 19);
        Student s2 = new Student("Filan2", "Fistek2", 21);
        Student s3 = new Student("Filan3", "Fistek3", 18);
        Student s4 = new Student("Filan4", "Fistek4", 22);
        Student s5 = new Student("Filan5", "Fistek5", 27);

        s1.setExams(List.of(e1, e5));
        s2.setExams(List.of(e4));
        s3.setExams(List.of(e1, e2, e3, e4, e5));
        s5.setExams(List.of(e2, e3, e5));

        session.persist(s1);
        session.persist(s2);
        session.persist(s3);
        session.persist(s4);
        session.persist(s5);

        transaction.commit();
        session.close();
    }
}
