package exercises.exercise1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class TestCustomer {

    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Customer.class)
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        Customer customer = new Customer("Filan", "Fisteku", "7335873857", "fl@gmail.com");

//        session.persist(customer);
//
//        customer.setPhoneNumber("123454678");
//        session.merge(customer);

        Customer found = session.find(Customer.class, 7);
        System.out.println(found);

        session.remove(found);

        found = session.find(Customer.class, 7);
        System.out.println(found);

        transaction.commit();

    }
}
