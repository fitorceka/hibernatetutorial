package exercises.exercise2;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class GenreRepository {

    private Session session;

    public GenreRepository(Session session) {
        this.session = session;
    }

    public void save(Genre genre) throws SQLException {
        Transaction transaction = session.beginTransaction();
        session.persist(genre);
        transaction.commit();
    }

    public void delete(Genre genre) throws SQLException {
        Transaction transaction = session.beginTransaction();
        session.remove(genre);
        transaction.commit();
    }

    public List<Genre> findByName(String name) throws SQLException {
        Transaction transaction = session.beginTransaction();
        String query = "select G from Genre G where G.name = :gName";
        Query<Genre> q = session.createQuery(query, Genre.class);
        q.setParameter("gName", name);
        List<Genre> all = q.getResultList();
        transaction.commit();

        return all;
    }

    public Optional<Genre> findById(int id) throws SQLException {
        Transaction transaction = session.beginTransaction();
        Optional<Genre> genre = Optional.of(session.find(Genre.class, id));
        transaction.commit();

        return genre;
    }

    public List<Genre> findAll() throws SQLException {
        Transaction transaction = session.beginTransaction();
        String query = "select G from Genre G";
        Query<Genre> q = session.createQuery(query, Genre.class);
        List<Genre> all = q.getResultList();
        transaction.commit();

        return all;
    }
}
