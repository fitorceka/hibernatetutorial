package exercises.exercise2;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;

import java.util.List;

@Entity
public class Movie extends Base {

    @Column(name = "TITLE")
    private String title;

    @Column(name = "YEAR_OF_RELEASE")
    private int yearOfRelease;

    @ManyToMany(mappedBy = "movies")
    private List<Actor> actors;

    @OneToMany(mappedBy = "movie")
    private List<Genre> genres;

    public Movie(String title, int yearOfRelease) {
        this.title = title;
        this.yearOfRelease = yearOfRelease;
    }

    public Movie() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }
}
