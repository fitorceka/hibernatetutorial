package exercises.exercise2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.SQLException;

public class Test {

    public static void main(String[] args) throws SQLException {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Actor.class)
                .addAnnotatedClass(Movie.class)
                .addAnnotatedClass(Genre.class)
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        GenreRepository genreRepository = new GenreRepository(session);

        System.out.println(genreRepository.findAll());
    }
}
