package onetomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Test {

    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        Car c1 = new Car("C class", 2020);
        Car c2 = new Car("E class", 2019);
        Car c3 = new Car("M BMW", 2022);
        Car c4 = new Car("RS6", 2016);
        Car c5 = new Car("TurboS", 2021);

        User user1 = new User("Filan1", "Fisteku1");
        User user2 = new User("Filan2", "Fisteku2");

        user1.setCars(List.of(c2, c3, c5));
        user2.setCars(List.of(c1, c4));

        session.persist(user1);
        session.persist(user2);

//        c1.setUser(user2);
//        c2.setUser(user1);
//        c3.setUser(user1);
//        c4.setUser(user2);
//        c5.setUser(user1);
//
//        session.persist(c1);
//        session.persist(c2);
//        session.persist(c3);
//        session.persist(c4);
//        session.persist(c5);

        transaction.commit();
        session.close();
    }
}
