package onetoone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        UserDetails userDetails = new UserDetails(27, "filan@gmail.com");
        User user = new User("Filan", "Fistekyu");
        user.setUserDetails(userDetails);

//        session.persist(userDetails);
//        session.persist(user);

        User found = session.find(User.class, 3);

        System.out.println(found);

        transaction.commit();
        session.close();
    }
}
