package hql;

import onetomany.Car;
import onetomany.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class TestJoin {

    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Car.class)
                .addAnnotatedClass(User.class)
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        String sql = "select U from User U join U.cars C where C.year > 2010";
        Query<User> q = session.createQuery(sql, User.class);
        List<User> users = q.getResultList();

        users.forEach(System.out::println);

        transaction.commit();

    }
}
