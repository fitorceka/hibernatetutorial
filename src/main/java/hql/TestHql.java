package hql;

import exercises.exercise1.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class TestHql {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Customer.class)
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        String str = "select C from Customer C";

        //  per te gjetur gjitha rezultatet
//        Query<Customer> query = session.createQuery(str, Customer.class);
//
//        List<Customer> all = query.getResultList();
//        all.forEach(System.out::println);
//
        // gjej gjith klientet qe kane adressen ne Kukes
//        String kukes = "select C.name, C.phoneNumber from Customer C where C.address = 'Kukes'";
//        Query<Object[]> query = session.createQuery(kukes, Object[].class);
//
//        List<Object[]> allKukes = query.getResultList();
//        allKukes.forEach(c -> {
//            for (Object o : Arrays.stream(c).toArray()) {
//                System.out.println(o);
//            }
//        });

//        System.out.println("-----------------------------------------------------");
//        System.out.println("Ordered Results");
//        System.out.println("-----------------------------------------------------");
//
//        String order = "select C from Customer C order by C.address DESC";
//        query = session.createQuery(order, Customer.class);
//        List<Customer> allOrdered = query.getResultList();
//        allOrdered.forEach(System.out::println);

        String findJohn = "select C from Customer C where C.name like '%John'";
        Query<Customer> query = session.createQuery(findJohn, Customer.class);

        List<Customer> allJohn = query.getResultList();
        System.out.println("All customers with name john");
        allJohn.forEach(System.out::println);

        transaction.commit();
    }
}
